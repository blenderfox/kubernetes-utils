SERVICE=nfs-kernel-server
PROCESSNAME=rpc.mountd

exportfs -ra

echo Starting rpcbind
mount -t nfsd nfsd /proc/fs/nfsd
service rpcbind start
mount -t nfsd nfsd /proc/fs/nfsd
service rpcbind restart

echo Starting service $SERVICE
service $SERVICE start
while [ true ];
do
  echo -n Checking if process $PROCESSNAME is running...
  if [ $(ps faxww | grep $PROCESSNAME | wc -l) -ge 2 ]; then
    echo Yes.
  else
    echo No. Restarting.
    service $SERVICE start
  fi
  sleep 30s
done
