rsync-capable container with auto-mounted S3 bucket embedded at /mnt/bucket. Needs to be run as privileged:

```
docker run -d -P --name s3fs --privileged -it \
  -e S3_BUCKET=my-backups-bucket \
  -e S3_REGION=eu-west-2 \
  -e AWSACCESSKEYID=my-aws-access-key-id \
  -e AWSSECRETACCESSKEY=my-secret-access-key \
  -e REMOTEKEY=https://path/to/my/public.key \
  -e BUCKETUSERPASSWORD=bucket-user-password-here
  blenderfox/s3fs
```

Find the port:
```
$ docker port s3fs
22/tcp -> 0.0.0.0:32771
```

Then ssh into the container using bucketuser user
```
$ ssh bucketuser@localhost -p 32771
Warning: Permanently added '[localhost]:32771' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 16.04 LTS (GNU/Linux 4.15.4-300.fc27.x86_64 x86_64)

 * Documentation:  https://help.ubuntu.com/

The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

bucketuser@95d8d20f5e17:~$ 
```

You can also use this with rsync, e.g.
```
$ rsync -rvhP /source/path bucketuser@container-host:/mnt/bucket/
```

This container requires the following environment variables mapped:

`S3_BUCKET`: AWS S3 bucket to map -- should be the bucket name, without the "s3://" bit

`S3_REGION`: The region which your bucket resides (e.g. eu-west-1)

`AWSACCESSKEYID`, `AWSSECRETACCESSKEY`: Your AWS API keys. The variable names are NOT mistyped -- s3fs needs these environment variables

`REMOTEKEY`: Remote location (curl/wget compatible) from which the container will attempt to download and replace the local authorized_keys file. Used for key-based ssh login. If the container cannot download this, it'll use password auth instead

`BUCKETUSERPASSWORD`: Password to use for the bucket user. Defaults to 'password' if not set (so make sure you set it)
