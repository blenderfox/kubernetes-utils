service ssh start
sed -i s/"#user_allow_other"/"user_allow_other"/g /etc/fuse.conf
useradd -d /home/bucketuser -m -s /bin/bash bucketuser
PASSWORD=$BUCKETUSERPASSWORD
if [ -z $PASSWORD ]; then
  echo Blank password provided, using "password"
  PASSWORD="password"
fi
echo -e "$PASSWORD\n$PASSWORD" | passwd bucketuser #Make sure you change this or use keys
echo Downloading key from $REMOTEKEY
wget $REMOTEKEY -O /home/bucketuser/.ssh/authorized_keys
chown --recursive bucketuser:bucketuser -v /home/bucketuser

# echo Key: $AWSACCESSKEYID
# echo Secret Key: $AWSSECRETACCESSKEY
# echo Password: $BUCKETUSERPASSWORD
# echo Remote Key: $REMOTEKEY

s3fs $S3_BUCKET /mnt/bucket -d -f -o endpoint=${S3_REGION},allow_other,uid=1000,gid=1000,max_stat_cache_size=1000,stat_cache_expire=900,retries=5,connect_timeout=10
