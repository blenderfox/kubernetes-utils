SERVICE=exim4
PROCESSNAME=exim4

echo Starting service $SERVICE
service $SERVICE start
while [ true ];
do
  echo -n Checking if process $PROCESSNAME is running... 
  if [ $(ps faxww | grep $PROCESSNAME | wc -l) -ge 2 ]; then
    echo Yes.
  else
    echo No. Restarting.
    service $SERVICE start
  fi
  sleep 30s
done
