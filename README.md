# kubernetes-utils
K8s Side Projects

This project contains files for some side projects I have been working on within Kubernetes

postfix: files to build a postfix MTA container (needs building twice due to the way postfix prompts during installation), and also it is better if you mount in a config file directly via ConfigMaps -- NOT READY

exim: files to build an exim MTA container -- NOT READY

nfs: files to build an NFS server -- NOT READY

s3fs: files to build an s3fs pod which you can rsync or SSH into and use almost like an NFS server (though without a RWM PV/PVC)
